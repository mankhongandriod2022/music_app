package com.mankhong.music_app

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.fragment.navArgs
import com.mankhong.music_app.data.DataSourse
import com.mankhong.music_app.data.DataSourse.songs
import com.mankhong.music_app.data.InventoryData
import com.mankhong.music_app.databinding.FragmentDetailBinding
import com.mankhong.music_app.model.Song


class DetailFragment : Fragment() {
    private  val navigationArgs: DetailFragmentArgs by navArgs()
    private var _binding: FragmentDetailBinding? = null
    private val binding get() = _binding
    private lateinit var song: Song
    private lateinit var newSong: Song


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        (activity as AppCompatActivity).supportActionBar!!.title = "Detail"
        _binding = FragmentDetailBinding.inflate(inflater, container, false)
        return binding?.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val id = navigationArgs.id

        song = DataSourse.getSong(id-1)
        if(song.bought == "n"){
            binding?.image?.setImageResource(song.imageResourceId)
            binding?.songName?.text = "Name : "+song.name
            binding?.songArtist?.text = "Artist : "+song.artist
            binding?.songType?.text = "Type : "+song.type
            binding?.songRDate?.text = "Release Date : "+song.releaseDate
            binding?.SongPrice?.text = "฿"+song.price
            binding?.PurchaseButton?.setOnClickListener{
                DataSourse.buy(id-1)
                InventoryData.add(song)
                var text = "Purchase successful"
                Toast.makeText(
                    requireContext(),
                    text,
                    Toast.LENGTH_SHORT
                ).show()
                val action = DetailFragmentDirections.actionDetailFragmentToInventoryFragment()
                view.findNavController().navigate(action)
            }
            binding?.BackButton?.setOnClickListener(){
                val action = DetailFragmentDirections.actionDetailFragmentToStoreFragment()
                view.findNavController().navigate(action)
            }
        }else if(song.bought == "y"){
            binding?.image?.setImageResource(song.imageResourceId)
            binding?.songName?.text = "Name : "+song.name
            binding?.songArtist?.text = "Artist : "+song.artist
            binding?.songType?.text = "Type : "+song.type
            binding?.songRDate?.text = "Release Date : "+song.releaseDate
            binding?.SongPrice?.text = "Purchased"
            binding?.PurchaseButton?.text = "Inventory"
            binding?.PurchaseButton?.setOnClickListener{
                val action = DetailFragmentDirections.actionDetailFragmentToInventoryFragment()
                view.findNavController().navigate(action)
            }
            binding?.BackButton?.setOnClickListener(){

                val action = DetailFragmentDirections.actionDetailFragmentToStoreFragment()
                view.findNavController().navigate(action)
            }
        }





    }

    companion object {

    }
}