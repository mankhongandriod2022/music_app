package com.mankhong.music_app.data

import com.mankhong.music_app.model.Song

object InventoryData {
     var inventory_song: MutableList<Song> = mutableListOf()

    fun add (song: Song){
        inventory_song.add(song)
    }

}