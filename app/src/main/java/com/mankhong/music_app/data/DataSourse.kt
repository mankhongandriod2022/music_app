package com.mankhong.music_app.data

import com.mankhong.music_app.R
import com.mankhong.music_app.model.Song

object DataSourse {

    val songs: MutableList<Song> = mutableListOf(
        Song(
            1,
            R.drawable.odo,
            "Odo",
            "Ado",
            "J-pop",
            49,
            "27/04/2021",
            "n"
        ),
        Song(
            2,
            R.drawable.missing,
            "Missing",
            "Ado",
            "J-pop",
            59,
            "10/10/2022",
            "n"
        ),
        Song(
            3,
            R.drawable.usseewa,
            "Usseewa",
            "Ado",
            "J-pop",
            89,
            "23/10/2020",
            "n"
        ),
        Song(
            4,
            R.drawable.kyougen,
            "Kyōgen",
            "Ado",
            "J-pop",
            199,
            "26/1/2022",
            "n"
        ),
        Song(
            5,
            R.drawable.shinkai,
            "Shinkai",
            "Eve",
            "J-pop",
            29,
            "4/12/2020",
            "n"
        ),
        Song(
            6,
            R.drawable.reb,
            "Rebellion",
            "Ado",
            "J-pop",
            39,
            "19/09/2022",
            "n"
        ),
        Song(
            7,
            R.drawable.killer_queen,
            "Killer Queen",
            "queen",
            "Rock",
            190,
            "13/05/2000",
            "n"
        ),


    )

    fun buy(pos: Int){
        songs[pos].bought = "y"
    }
    fun getSong(pos: Int):Song{
        return songs[pos]
    }


}