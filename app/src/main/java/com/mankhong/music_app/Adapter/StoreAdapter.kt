package com.mankhong.music_app.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.mankhong.music_app.R
import com.mankhong.music_app.model.Song

class StoreAdapter(val context: Context, private val songList:List<Song> , private val onItemClicked: (Song) -> Unit):
    RecyclerView.Adapter<StoreAdapter.StoreViewHolder>(){
        class StoreViewHolder(private val view: View):
                RecyclerView.ViewHolder(view){
                    val image : ImageView = view.findViewById(R.id.songImage)
                    val name  : TextView  = view.findViewById(R.id.songName)
                    val artist: TextView = view.findViewById(R.id.songArtist)
                    val price  : TextView  = view.findViewById(R.id.SongPrice)
                    val type  : TextView  = view.findViewById(R.id.SongType)
                }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StoreViewHolder {
        val adapterLayout = LayoutInflater.from(
            parent.context
        ).inflate(R.layout.store_list,parent,false)
        return StoreViewHolder(adapterLayout)
    }

    override fun onBindViewHolder(holder: StoreViewHolder, position: Int) {
        val item = songList[position]
        if(item.bought == "n") {
            holder.image.setImageResource(item.imageResourceId)
            holder.name.text = item.name
            holder.price.text = "฿"+item.price.toString()
            holder.artist.text = item.artist
            holder.type.text = item.type
            holder.itemView.setOnClickListener{
                onItemClicked(item)
            }
        }else if(item.bought == "y"){
            holder.image.setImageResource(item.imageResourceId)
            holder.name.text = item.name
            holder.price.text = "purchased"
            holder.artist.text = item.artist
            holder.type.text = item.type
            holder.itemView.setOnClickListener{
                onItemClicked(item)
            }
        }


    }

    override fun getItemCount() = songList.size
}

